﻿namespace StockAdvisoryModels;

public record StockRecord
{
    public string Company { get; init; } = default!;
    public string Ticker { get; init; } = default!;
    public string CuspId { get; init; } = default!;
    public ulong Shares { get; init; } = default!;
    public decimal Weight { get; init; } = default!;
}