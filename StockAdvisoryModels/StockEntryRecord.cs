using System.ComponentModel.DataAnnotations;

namespace StockAdvisoryModels;

public record StockEntryRecord : StockRecord
{
    public DateTime Date { get; init; } = default!;
    
    [Display(Name = "US$")]
    public decimal MarketValue { get; init; }
}
