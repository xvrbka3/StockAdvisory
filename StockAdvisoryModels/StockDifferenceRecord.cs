﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockAdvisoryModels;

public record StockDifferenceRecord : StockRecord
{
    public float SharesPercentChange { get; init; } = default!;
    
}

