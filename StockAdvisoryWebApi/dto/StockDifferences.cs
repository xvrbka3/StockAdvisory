using StockAdvisoryModels;

namespace StockAdvisoryWebApi.dto;

public class StockDifferences
{
    public IEnumerable<StockEntryRecord> AddedStocks { get; init; } = new List<StockEntryRecord>();
    public IEnumerable<StockEntryRecord> RemovedStocks { get; init; } = new List<StockEntryRecord>();
    public IEnumerable<StockDifferenceRecord> DifferentStocks { get; init; } = new List<StockDifferenceRecord>();
}