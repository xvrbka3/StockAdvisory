using dotenv.net;
using dotenv.net.Utilities;
using NodaTime;
using StockAdvisory;

namespace StockAdvisoryWebApi;

public class Program
{
    static void Main(string[] args)
    {
        DotEnv.Load();
        
        var builder = WebApplication.CreateBuilder(args);
        var fileManager = new FileManager("storage", new HttpClient(), EnvReader.GetStringValue("STOCK_SOURCE_URL"), SystemClock.Instance);

        // Add services to the container.

        builder.Services.AddControllers();
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        builder.Services.AddSingleton<HttpClient>();
        builder.Services.AddSingleton<IFileManager>(fileManager);
        builder.Services.AddSingleton<IStockParser, StockParser>();
        builder.Services.AddSingleton<IStockFormatter, StockTextFormatter>();
        builder.Services.AddSingleton<IStockDifferenceAnalyzerFactory, StockDifferenceAnalyzerFactory>();
        builder.Services.AddSingleton<IClock>(SystemClock.Instance);

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();

        app.UseAuthorization();

        app.MapControllers();
        
        // register periodic file download 
        var downloadCancelToken = new CancellationToken();
        var downloaderService = new DownloaderService(fileManager, 24);
        downloaderService.StartAsync(downloadCancelToken);

        app.Run(); 
    }
}
