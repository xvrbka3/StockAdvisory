using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Mvc;
using NodaTime;
using StockAdvisory;
using StockAdvisoryWebApi.dto;

namespace StockAdvisoryWebApi.Controllers;

[ApiController]
[Route("[controller]")]
public class StockAdvisoryController : ControllerBase
{
    private readonly IFileManager _fileManager;
    private readonly IStockParser _stockParser;
    private readonly IStockDifferenceAnalyzerFactory _stockDifferenceAnalyzerFactory;
    private readonly IClock _clock;

    public StockAdvisoryController(IFileManager fileManager, IStockParser stockParser,
        IStockDifferenceAnalyzerFactory stockDifferenceAnalyzerFactory, IClock clock)
    {
        _fileManager = fileManager;
        _stockParser = stockParser;
        _stockDifferenceAnalyzerFactory = stockDifferenceAnalyzerFactory;
        _clock = clock;
    }

    /// <summary>
    /// Gets differences between stocks on two days
    /// </summary>
    /// <param name="date1"></param>
    /// <param name="date2"></param>
    /// <returns></returns>
    [HttpGet(Name = "Get stock differences between two dates")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(StockDifferences))]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetDifferences([FromQuery] DateTime? date1, [FromQuery] DateTime? date2)
    {
        var diff = await GetStockDifferencesFromDates(date1, date2);

        if (diff is not null)
            return Ok(diff);

        return NotFound();
    }
    
    private async Task<StockDifferences?> GetStockDifferencesFromDates(DateTime? date1, DateTime? date2)
    {
        if (date1 is null && date2 is null)
            (date1, date2) = SetLastTwoDates();
        else if (date1 is null || date2 is null)
            return null;
        
        var path1 = _fileManager.GetRecordFilePath((DateTime) date1);
        var path2 = _fileManager.GetRecordFilePath((DateTime) date2);
        
        if (path1 is null && IsToday(date1)) //today stocks are not downloaded yet
            (path1, _) = await _fileManager.DownloadLatest();

        if (path1 == null || path2 == null)
            return null;

        var analyzer = _stockDifferenceAnalyzerFactory.CreateAnalyzer(
            _stockParser.Parse(path2),
            _stockParser.Parse(path1)
        );

        return new StockDifferences
        {
            AddedStocks = analyzer.AddedStocks(),
            RemovedStocks = analyzer.RemovedStocks(),
            DifferentStocks = analyzer.ChangedPositions()
        };
    }

    private (DateTime today, DateTime yesterday) SetLastTwoDates()
    {
        var today = _clock.GetCurrentInstant().ToDateTimeUtc().Date;
        var yesterday = _clock.GetCurrentInstant().ToDateTimeUtc().Date.Subtract(TimeSpan.FromDays(1));
        return (today, yesterday);
    }

    private bool IsToday([DisallowNull] DateTime? date1)
    {
        return date1.Equals(_clock.GetCurrentInstant().ToDateTimeUtc().Date);
    }
}
