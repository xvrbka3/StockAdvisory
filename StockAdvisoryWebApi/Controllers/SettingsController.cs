using Microsoft.AspNetCore.Mvc;
using StockAdvisory;
using StockAdvisoryWebApi.dto;

namespace StockAdvisoryWebApi.Controllers;

[ApiController]
[Route("[controller]")]
public class StockSettingsController : ControllerBase
{
    private readonly IFileManager _fileManager;
    
    public StockSettingsController(IFileManager fileManager)
    {
        _fileManager = fileManager;
    }
    
    /// <summary>
    /// Sets location to store files with stock records
    /// </summary>
    /// <param name="location"></param>
    /// <returns></returns>
    [HttpPost(Name = "Change storage path to store files with records")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public IActionResult ChangeStorageLocation(StockStorageLocation location)
    {
        _fileManager.SetStorageLocation(location.Location);
        return Ok();
    }
}