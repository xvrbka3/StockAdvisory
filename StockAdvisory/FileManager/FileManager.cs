using System.Globalization;
using NodaTime;

namespace StockAdvisory;

public class FileManager : IFileManager
{
    private string _storagePath;

    public string StoragePath
    {
        get { return _storagePath; }
    }

    private readonly string _sourceUrl;
    
    private readonly HttpClient _httpClient;
    
    private const string DatetimeFormat = "yyyy-MM-dd-HH_mm_ss";

    private readonly IClock _clock;

    public FileManager(string storagePath, HttpClient downloader, string sourceUrl, IClock clock)
    {
        _storagePath = storagePath;
        _sourceUrl = sourceUrl;
        _httpClient = downloader;
        _httpClient.DefaultRequestHeaders.UserAgent.ParseAdd("StockAdvisory/0.1");
        _clock = clock;
        SetStorageLocation(storagePath);
    }

    private string ConstructFilePath(string fileName, string ext)
    {
        return _storagePath + Path.DirectorySeparatorChar + fileName + ext;
    }

    private IEnumerable<string> GetFilesFromStorage()
    {
        try
        {
            return Directory.GetFiles(_storagePath);
        }         
        catch (IOException e)
        {
            Console.Error.WriteLine(e);
            return new List<string>();
        }
    }

    public async Task<(string? filePath, DateTime dateTime)> DownloadLatest()
    {
        var now = _clock.GetCurrentInstant().ToDateTimeUtc();
        try
        {
            var filePath = ConstructFilePath(now.ToString(DatetimeFormat), ".csv");

            var responseStream = await _httpClient.GetStreamAsync(_sourceUrl);
            await using var fileStream = new FileStream(filePath, FileMode.Create);
            await responseStream.CopyToAsync(fileStream);

            return (filePath, now);
        }
        catch (Exception e)
        {
            Console.Error.WriteLine(e);
            return (null, now);
        }
    }

    public ICollection<DateTime> ListAvailableRecords()
    {
        var output = new List<DateTime>();
        try
        {
            var files = GetFilesFromStorage();

            foreach (var file in files)
            {
                var withoutExtension = Path.GetFileNameWithoutExtension(file);
                try
                {
                    var date = DateTime.ParseExact(withoutExtension, DatetimeFormat, CultureInfo.InvariantCulture);
                    output.Add(date);
                }
                catch (FormatException)
                {
                    Console.Error.WriteLine($"Invalid record in storage: {file}");
                }
            }

            return output;
        }
        catch (Exception e)
        {
            Console.Error.WriteLine(e);
            return output;
        }
    }

    public string? GetRecordFilePath(DateTime record)
    {
        var filePath = ConstructFilePath(record.ToString(DatetimeFormat), ".csv");
        var files = GetFilesFromStorage();
        return !files.Contains(filePath) ? null : filePath;
    }

    public void SetStorageLocation(string location)
    {
        _storagePath = Path.GetFullPath(location);
        Directory.CreateDirectory(_storagePath);
    }
}