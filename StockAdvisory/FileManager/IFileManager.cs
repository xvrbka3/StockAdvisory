namespace StockAdvisory;

public interface IFileManager
{
    void SetStorageLocation(string location);
    Task<(string? filePath, DateTime dateTime)> DownloadLatest();
    ICollection<DateTime> ListAvailableRecords();
    string? GetRecordFilePath(DateTime record);
}