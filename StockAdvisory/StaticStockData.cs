using StockAdvisoryModels;

namespace StockAdvisory;

public class StaticStockData
{
    public static List<StockEntryRecord> EarlierRecords { get; } = new List<StockEntryRecord>()
    {
        new StockEntryRecord()
        {
            Date = new DateTime(2022, 3, 31),
            Company = "TESLA INC",
            Ticker = "TSLA",
            CuspId = "88160R101",
            Shares = 1_129_150,
            MarketValue = 1_235_278_808.5M,
            Weight = 33.15M,
        },
        new StockEntryRecord()
        {
            Date = new DateTime(2022, 3, 31),
            Company = "COINBASE GLOBAL INC -CLASS A",
            Ticker = "COIN",
            CuspId = "19260Q107",
            Shares = 4_314_674,
            MarketValue = 848_696_375.8M,
            Weight = 22.77M,
        },
        new StockEntryRecord()
        {
            Date = new DateTime(2022, 3, 31),
            Company = "TELADOC HEALTH INC",
            Ticker = "TDOC",
            CuspId = "87918A105",
            Shares = 11_286_751,
            MarketValue = 824_722_895.57M,
            Weight = 22.13M,
        },
        new StockEntryRecord()
        {
            Date = new DateTime(2022, 3, 31),
            Company = "ROKU INC",
            Ticker = "ROKU",
            CuspId = "77543R102",
            Shares = 6_296_234,
            MarketValue = 818_195_608.3M,
            Weight = 21.95M,
        },
    };

    public static List<StockEntryRecord> LaterRecords { get; } = new List<StockEntryRecord>()
    {
        new StockEntryRecord()
        {
            Date = new DateTime(2022, 4, 11),
            Company = "TESLA INC",
            Ticker = "TSLA",
            CuspId = "88160R101",
            Shares = 1_063_294,
            MarketValue = 1_090_397_364.06M,
            Weight = 34.37M,
        },
        new StockEntryRecord()
        {
            Date = new DateTime(2022, 4, 11),
            Company = "TELADOC HEALTH INC",
            Ticker = "TDOC",
            CuspId = "87918A105",
            Shares = 10_564_463,
            MarketValue = 697_888_425.78M,
            Weight = 22M,
        },
        new StockEntryRecord()
        {
            Date = new DateTime(2022, 4, 11),
            Company = "ROKU INC",
            Ticker = "ROKU",
            CuspId = "77543R102",
            Shares = 6_050_785,
            MarketValue = 694_267_070.9M,
            Weight = 21.9M,
        },
        new StockEntryRecord()
        {
            Date = new DateTime(2022, 4, 11),
            Company = "ZOOM VIDEO COMMUNICATIONS-A",
            Ticker = "ZM",
            CuspId = "98980L101",
            Shares = 6_209_236,
            MarketValue = 688_604_272.40M,
            Weight = 21.73M,
        },
    };
}