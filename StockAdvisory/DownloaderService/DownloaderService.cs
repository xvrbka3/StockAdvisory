using Microsoft.Extensions.Hosting;

namespace StockAdvisory;

public class DownloaderService : IHostedService
{
    private Timer? _timer;
    private readonly IFileManager _fileManager;
    private readonly double _interval;

    public DownloaderService(IFileManager manager, double interval)
    {
        _fileManager = manager;
        _interval = interval;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        _timer = new Timer(
            DownloadFile, 
            null, 
            TimeSpan.Zero, 
            TimeSpan.FromHours(_interval)
        );

        return Task.CompletedTask;
    }

    private async void DownloadFile(object? state)
    {
        await _fileManager.DownloadLatest();
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        _timer?.Dispose();
        return Task.CompletedTask;
    }

    public ICollection<DateTime> ListDownloadedRecords()
    {
        return _fileManager.ListAvailableRecords();
    }
}