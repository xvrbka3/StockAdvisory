using StockAdvisoryModels;

namespace StockAdvisory;

public class StockTextFormatter : IStockFormatter
{
    private const int SmallAlign = -12;
    private const int BigAlign = -30;

    public string Format(StockEntryRecord record)
    {
        return $"{record.Company,BigAlign}{record.Ticker,SmallAlign}" +
               $"{record.Shares,BigAlign}{record.Weight + "%",SmallAlign}";
    }

    public string Format(StockDifferenceRecord record)
    {
        var changeSymbol = GetChangeSymbol(record.SharesPercentChange);
        var percentage = $"{record.SharesPercentChange:0.##}";

        return $"{record.Company,BigAlign}{record.Ticker,SmallAlign}" +
               $"{record.Shares + $" ({changeSymbol + percentage + "%)"}",BigAlign}" +
               $"{record.Weight + "%", SmallAlign}";
    }

    private static string GetChangeSymbol(float change)
    {
        if (change != 0)
        {
            return change > 0 ? " ▲ " : " ▼ ";
        }
        return " — ";
    }

    private static string FormatHeader(bool includeChange)
    {
        var shares = "Shares" + (includeChange ? " (Change)" : "");
        
        return $"{"Company",BigAlign}{"Ticker",SmallAlign}" +
               $"{shares,BigAlign}{"Weight",SmallAlign}" + 
               Environment.NewLine;
    }

    public string Format(IEnumerable<StockEntryRecord> records)
    {
        return FormatHeader(false) +
               string.Join(Environment.NewLine, records.Select(Format));
    }

    public string Format(IEnumerable<StockDifferenceRecord> records)
    {
        return FormatHeader(true) +
               string.Join(Environment.NewLine, records.Select(Format));
    }
}