using StockAdvisoryModels;

namespace StockAdvisory;

public interface IStockFormatter
{
    public string Format(StockDifferenceRecord record);

    public string Format(StockEntryRecord record);

    public string Format(IEnumerable<StockEntryRecord> records);
    
    public string Format(IEnumerable<StockDifferenceRecord> records);
}