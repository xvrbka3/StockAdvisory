using StockAdvisoryModels;

namespace StockAdvisory;

public interface IStockDifferenceAnalyzerFactory
{
    IStockDifferenceAnalyzer CreateAnalyzer(ICollection<StockEntryRecord> previous,
        ICollection<StockEntryRecord> current);
}