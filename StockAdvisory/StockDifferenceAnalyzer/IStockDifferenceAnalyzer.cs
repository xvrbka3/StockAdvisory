using StockAdvisoryModels;

namespace StockAdvisory;

public interface IStockDifferenceAnalyzer
{
    ICollection<StockEntryRecord> Previous { get; }
    ICollection<StockEntryRecord> Current { get; }

    /// <summary>
    /// Returns all stocks between previous and next, preferring later entries
    /// </summary>
    /// <returns>Collection of all stocks</returns>
    ICollection<StockEntryRecord> AllStocks();

    ICollection<StockEntryRecord> AddedStocks();
    ICollection<StockEntryRecord> RemovedStocks();

    /// <summary>
    /// Returns all changed positions. Positions with positive SharesPercentChange are increased, the ones with 
    /// negative are decreased. Does not return new positions or removed positions.
    /// </summary>
    /// <returns>List of StockDifferenceRecord</returns>
    IEnumerable<StockDifferenceRecord> ChangedPositions();
}