using StockAdvisoryModels;

namespace StockAdvisory;

public class StockDifferenceAnalyzerFactory : IStockDifferenceAnalyzerFactory
{
    public IStockDifferenceAnalyzer CreateAnalyzer(ICollection<StockEntryRecord> previous,
        ICollection<StockEntryRecord> current)
    {
        return new StockDifferenceAnalyzer(previous, current);
    }
}