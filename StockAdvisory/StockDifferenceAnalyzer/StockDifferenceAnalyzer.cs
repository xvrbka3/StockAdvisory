using System.Linq;
using StockAdvisoryModels;

namespace StockAdvisory;

public class StockDifferenceAnalyzer : IStockDifferenceAnalyzer
{
    public ICollection<StockEntryRecord> Previous { get; }
    public ICollection<StockEntryRecord> Current { get; }

    public StockDifferenceAnalyzer(ICollection<StockEntryRecord> previous, ICollection<StockEntryRecord> current)
    {
        if (!AllCuspIdsAreUnique(previous) || !AllCuspIdsAreUnique(current))
            throw new ArgumentException("One of the argument contains duplicate");
        Previous = previous;
        Current = current;
    }

    /// <summary>
    /// Returns true if enumerable contains all records with unique CuspId
    /// </summary>
    /// <returns>Boolean</returns>
    private static bool AllCuspIdsAreUnique(IEnumerable<StockEntryRecord> enumerable)
    {
        return enumerable.GroupBy(s => s.CuspId).All(g => g.Count() == 1);
    }

    /// <summary>
    /// Returns all stocks between previous and next, preferring later entries
    /// </summary>
    /// <returns>Collection of all stocks</returns>
    public ICollection<StockEntryRecord> AllStocks()
    {
        return Previous
            .Concat(Current)
            .GroupBy(entry => entry.CuspId, (_, records) => records.MaxBy(record => record.Date)!)
            .ToList();
    }

    public ICollection<StockEntryRecord> AddedStocks()
    {
        return Current.Where(entry => Previous.All(entryPrev => entryPrev.CuspId != entry.CuspId)).ToList();
    }

    public ICollection<StockEntryRecord> RemovedStocks()
    {
        return Previous.Where(entry => Current.All(entryCurr => entryCurr.CuspId != entry.CuspId)).ToList();
    }

    /// <summary>
    /// Returns all changed positions. Positions with positive SharesPercentChange are increased, the ones with 
    /// negative are decreased. Does not return new positions or removed positions.
    /// </summary>
    /// <returns>List of StockDifferenceRecord</returns>
    public IEnumerable<StockDifferenceRecord> ChangedPositions()
    {
        return from entry in Current
            let oldRecord = Previous.Where(entryPrev => entryPrev.CuspId == entry.CuspId).ToList()
            where oldRecord.Count == 1 && oldRecord.First().Shares != entry.Shares
            select new StockDifferenceRecord()
            {
                Company = entry.Company,
                Ticker = entry.Ticker,
                CuspId = entry.CuspId,
                Shares = entry.Shares,
                SharesPercentChange = (entry.Shares - (float) oldRecord.First().Shares) / oldRecord.First().Shares * 100,
                Weight = entry.Weight
            };
    }
}