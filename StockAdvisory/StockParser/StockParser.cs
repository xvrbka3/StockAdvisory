using System.Globalization;
using CsvHelper;
using StockAdvisoryModels;

namespace StockAdvisory;

public class StockParser : IStockParser
{
    public ICollection<StockEntryRecord> Parse(string filePath)
    {
        var records = new List<StockEntryRecord>();

        try
        {
            using var reader = new StreamReader(filePath);
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            {
                csv.Read();
                csv.ReadHeader();

                while (csv.Read())
                {
                    try
                    {
                        var record = new StockEntryRecord
                        {
                            Date = csv.GetField<DateTime>("date"),
                            Company = csv.GetField("company"),
                            Ticker = csv.GetField("ticker"),
                            CuspId = csv.GetField("cusip"),
                            Shares = ulong.Parse(csv.GetField("shares").Replace(",", "")),
                            MarketValue = decimal.Parse(csv.GetField("market value ($)").Replace("$", "")),
                            Weight = decimal.Parse(csv.GetField("weight (%)").Replace("%", "")),
                        };
                        records.Add(record);
                    }
                    // If there is a row that does not have the expected format, just skip the row
                    catch (FormatException)
                    {
                    }
                    catch (Exception e)
                    {
                        Console.Error.WriteLine(e);
                    }
                }
                return records;
            }
        }
        catch (FileNotFoundException e)
        {
            Console.Error.WriteLine(e);
            return records;
        }
    }
}