using StockAdvisoryModels;

namespace StockAdvisory;

public interface IStockParser
{
    ICollection<StockEntryRecord> Parse(string filePath);
}