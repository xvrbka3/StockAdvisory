﻿// See https://aka.ms/new-console-template for more information

using dotenv.net;
using dotenv.net.Utilities;
using NodaTime;
using StockAdvisory;

namespace ConsoleFrontend;

class Program
{
    static async Task Main(string[] args)
    {
        DotEnv.Load();
        
        var httpClient = new HttpClient();
        var fileManager = new FileManager("storage", httpClient, EnvReader.GetStringValue("STOCK_SOURCE_URL"), SystemClock.Instance);

        var result = await fileManager.DownloadLatest();
        Console.WriteLine("Downloaded new record to:");
        Console.WriteLine(result);
        var downloaded = fileManager.ListAvailableRecords();
        
        Console.WriteLine("Available records are:");
        foreach (var item in downloaded)
        {
            Console.WriteLine(item.ToString("yyyy-MM-dd-HH_mm"));
        }
        
        Console.WriteLine();

        var stockParser = new StockParser();
        var newData = stockParser.Parse(fileManager.GetRecordFilePath(downloaded.Last())!);
        var oldData = stockParser.Parse(fileManager.GetRecordFilePath(downloaded.First())!);
        var analyzer = new StockDifferenceAnalyzer(oldData, newData);
        var formatter = new StockTextFormatter();
        Console.WriteLine("Added stocks:");
        Console.WriteLine(formatter.Format(analyzer.AddedStocks()));
        Console.WriteLine("Removed stocks:");
        Console.WriteLine(formatter.Format(analyzer.RemovedStocks()));
        Console.WriteLine("Changed stocks:");
        Console.WriteLine(formatter.Format(analyzer.ChangedPositions()));
    }
}
