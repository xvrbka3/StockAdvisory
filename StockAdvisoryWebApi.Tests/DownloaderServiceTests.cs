using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using NodaTime;
using NUnit.Framework;
using JustEat.HttpClientInterception;
using StockAdvisory;

namespace StockAdvisoryWebApi.Tests;

public class StockDownloaderServiceTests
{
    private DownloaderService _downloaderService;

    [SetUp]
    public void Setup()
    {
        //prepare filesystem
        try
        {
            Directory.Delete("filesystem", true);
        }
        catch (DirectoryNotFoundException)
        {
        }
        Directory.CreateDirectory("filesystem");
        
        //create a mock httpclient that will respond with test .csv on request
        var options = new HttpClientInterceptorOptions();
        var httpBuilder = new HttpRequestInterceptionBuilder();
        var csvString = File.ReadAllText($"testfiles{Path.DirectorySeparatorChar}etf.csv");
        httpBuilder
            .Requests()
            .ForGet()
            .ForHttps()
            .ForHost("ark-funds.com")
            .Responds()
            .WithContent(csvString)
            .RegisterWith(options);

        var downloadUrl = "https://ark-funds.com/";
        var fileManager = new FileManager($"filesystem", options.CreateHttpClient(), downloadUrl, SystemClock.Instance);
        _downloaderService = new DownloaderService(fileManager, 0.001); // 3.6 second interval
    }

    [TearDown]
    public void Teardown()
    {
        try
        {
            Directory.Delete("filesystem", true);
        }
        catch (DirectoryNotFoundException)
        {
        }
    }
    
    [Test]
    public void Test_When_ServiceRuns_Then_DownloadIsPeriodic()
    {
        var now = SystemClock.Instance.GetCurrentInstant().ToDateTimeUtc();
        _downloaderService.StartAsync(new CancellationToken());
        
        //sleep for 6 seconds, should download atleast once
        Thread.Sleep(6000);
        
        _downloaderService.StopAsync(new CancellationToken());
        
        var downloadedRecords = _downloaderService.ListDownloadedRecords();
        Assert.IsNotEmpty(downloadedRecords);
        Assert.AreEqual(2, downloadedRecords.Count);
        foreach (var record in downloadedRecords)
        {
            Assert.That(record, Is.EqualTo(now).Within(TimeSpan.FromSeconds(30)));
        }
    }
}