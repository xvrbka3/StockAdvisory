using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Json;
using System.Threading.Tasks;
using JustEat.HttpClientInterception;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using StockAdvisory;
using NodaTime;
using NodaTime.Testing;
using StockAdvisoryWebApi.dto;
using System.Net.Http;

namespace StockAdvisoryWebApi.Tests;

public class StockWebApiTests
{
    private WebApplicationFactory<Program> _factory;
    private FakeClock _clock;
    private FileManager _fileManager;

    private void SetupFiles()
    {
        try
        {
            Directory.Delete("filesystem", true);
        }
        catch (DirectoryNotFoundException)
        {
        }
        Directory.CreateDirectory("filesystem");

        string[] sourceNames = {"ark_holdings_apr11.csv", "ark_holdings_mar31.csv"};
        string[] destNames = {"2022-04-11-00_00_00.csv", "2022-03-31-00_00_00.csv"};
        foreach (var (sourceName, destName) in sourceNames.Zip(destNames))
        {
            var sourcePath = $"csv{Path.DirectorySeparatorChar}{sourceName}";
            var destPath = $"filesystem{Path.DirectorySeparatorChar}{destName}";
            File.Copy(sourcePath, destPath);
        }

    }

    [SetUp]
    public void Setup()
    {
        SetupFiles();
        _clock = new FakeClock(Instant.FromUtc(2022, 4, 12, 0, 0));
        
        var options = new HttpClientInterceptorOptions();
        var httpBuilder = new HttpRequestInterceptionBuilder();
        var csvString = File.ReadAllText($"testfiles{Path.DirectorySeparatorChar}etf.csv");

        httpBuilder
            .Requests()
            .ForGet()
            .ForHttps()
            .ForHost("ark-funds.com")
            .Responds()
            .WithContent(csvString)
            .RegisterWith(options);

        // It really does not matter
        var downloadUrl = "https://ark-funds.com/";
        _fileManager = new FileManager($"filesystem", options.CreateHttpClient(), downloadUrl, _clock);

        _factory = new WebApplicationFactory<Program>()
            .WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    var descriptor = services.Single(s => s.ServiceType == typeof(IFileManager));
                    Assert.True(services.Remove(descriptor));

                    descriptor = services.Single(s => s.ServiceType == typeof(IClock));
                    Assert.True(services.Remove(descriptor));

                    services.AddSingleton<IFileManager>(_fileManager);
                    services.AddSingleton<IClock>(_clock);
                });
            });
    }

    [TearDown]
    public void Teardown()
    {
        try
        {
            Directory.Delete("filesystem", true);
        }
        catch (DirectoryNotFoundException)
        {
        }
    }

    [Test]
    public async Task Test_WhenNoDatesArePassed_And_NoFilesExist_Then_NotFoundIsReturned()
    {
        // no files should exist
        Teardown();
        
        var client = _factory.CreateClient();

        var response = await client.GetAsync("/StockAdvisory");
        Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        
        // create files again for other tests
        SetupFiles();
    }
    
    [Test]
    public async Task Test_WhenTwoDatesArePassed_And_FilesDoesNotExist_Then_NotFoundIsReturned()
    {
        var client = _factory.CreateClient();
        
        //first file
        var parameters = "date1=2022-04-11T00:00:00.000Z&date2=2010-03-03T00:00:00.000Z";
        var response = await client.GetAsync($"/StockAdvisory?{parameters}");
        Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        
        //second file
        parameters = "date1=2022-04-10T00:00:00.000Z&date2=2022-04-11T00:00:00.000Z";
        response = await client.GetAsync($"/StockAdvisory?{parameters}");
        Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        
        //both files
        parameters = "date1=2022-04-10T00:00:00.000Z&date2=2022-04-15T00:00:00.000Z";
        response = await client.GetAsync($"/StockAdvisory?{parameters}");
        Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
    }
    
    [Test]
    public async Task Test_WhenNoDatesArePassed_Then_LastTwoDaysAreCompared()
    {
        var client = _factory.CreateClient();
        var stockParser = new StockParser();
        
        var response = await client.GetAsync($"/StockAdvisory?");
        
        Assert.True(response.IsSuccessStatusCode);
        var returned = await response.Content.ReadFromJsonAsync<StockDifferences>();
        Assert.NotNull(returned);
        
        var analyzer = new StockDifferenceAnalyzerFactory()
            .CreateAnalyzer(
                stockParser.Parse($"filesystem{Path.DirectorySeparatorChar}2022-04-11-00_00_00.csv"),
                stockParser.Parse($"filesystem{Path.DirectorySeparatorChar}2022-04-12-00_00_00.csv")
            );

        var expected = new StockDifferences
        {
            AddedStocks = analyzer.AddedStocks(),
            RemovedStocks = analyzer.RemovedStocks(),
            DifferentStocks = analyzer.ChangedPositions()
        };

        CollectionAssert.AreEqual(expected.AddedStocks, returned.AddedStocks);
        CollectionAssert.AreEqual(expected.RemovedStocks, returned.RemovedStocks);
        CollectionAssert.AreEqual(expected.DifferentStocks, returned.DifferentStocks);
    }
    
    [Test]
    public async Task Test_WhenTwoDatesArePassed_Then_TwoDaysAreCompared()
    {
        var client = _factory.CreateClient();
        
        var stockParser = new StockParser();
        var analyzer = new StockDifferenceAnalyzerFactory()
            .CreateAnalyzer(
                stockParser.Parse($"filesystem{Path.DirectorySeparatorChar}2022-03-31-00_00_00.csv"),
                stockParser.Parse($"filesystem{Path.DirectorySeparatorChar}2022-04-11-00_00_00.csv")
            );

        var expected = new StockDifferences
        {
            AddedStocks = analyzer.AddedStocks(),
            RemovedStocks = analyzer.RemovedStocks(),
            DifferentStocks = analyzer.ChangedPositions()
        };
        

        var parameters = "date1=2022-04-11T00:00:00.000Z&date2=2022-03-31T00:00:00.000Z";
        var response = await client.GetAsync($"/StockAdvisory?{parameters}");
        
        Assert.True(response.IsSuccessStatusCode);
        var returned = await response.Content.ReadFromJsonAsync<StockDifferences>();
        Assert.NotNull(returned);

        CollectionAssert.AreEqual(expected.AddedStocks, returned.AddedStocks);
        CollectionAssert.AreEqual(expected.RemovedStocks, returned.RemovedStocks);
        CollectionAssert.AreEqual(expected.DifferentStocks, returned.DifferentStocks);
    }
    
    [Test]
    public async Task Test_WhenOneOfDatesAreMissing_Then_NotFoundIsReturned()
    {
        var client = _factory.CreateClient();
        
        //first date
        var parameters = "date1=&date2=2010-03-03T00:00:00.000Z";
        var response = await client.GetAsync($"/StockAdvisory?{parameters}");
        Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        
        parameters = "date2=2010-03-03T00:00:00.000Z";
        response = await client.GetAsync($"/StockAdvisory?{parameters}");
        Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        
        //second date
        parameters = "date1=2010-03-03T00:00:00.000Z&date2=";
        response = await client.GetAsync($"/StockAdvisory?{parameters}");
        Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        
        parameters = "date1=2010-03-03T00:00:00.000Z";
        response = await client.GetAsync($"/StockAdvisory?{parameters}");
        Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
    }


    [Test]
    public async Task Test_WhenPathIsEmpty_Then_ArgumentExceptionIsThrown()
    {
        var client = _factory.CreateClient();
        Microsoft.AspNetCore.Mvc.OkResult response = null;
        StockStorageLocation stockStorageLocation = new StockStorageLocation();
        stockStorageLocation.Location = "";
        var jsonContent = JsonContent.Create(stockStorageLocation);

        try
        {
            await client.PostAsync("/StockSettings", jsonContent);
        }
        catch (System.Exception err)
        {
            Assert.IsTrue(err is System.ArgumentException);
        }

        Assert.AreEqual(response, null);
    }

    [Test]
    public async Task Test_WhenChangingDirectory_Then_OkIsReturned()
    {
        Directory.CreateDirectory("filesystem2");
        var client = _factory.CreateClient();
        StockStorageLocation stockStorageLocation = new StockStorageLocation();
        stockStorageLocation.Location = "filesystem2";
        var jsonContent = JsonContent.Create(stockStorageLocation);
        var response = await client.PostAsync("/StockSettings", jsonContent);
        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

        // set back to "filesystem"
        stockStorageLocation.Location = "filesystem";
        jsonContent = JsonContent.Create(stockStorageLocation);
        response = await client.PostAsync("/StockSettings", jsonContent);
        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

        Directory.Delete("filesystem2", true);
    }

    [Test]
    public async Task Test_WhenChangingDirectory_Then_StoragePathChanges()
    {
        Directory.CreateDirectory("filesystem2");
        var client = _factory.CreateClient();
        var savedStoragePath = _fileManager.StoragePath;
        StockStorageLocation stockStorageLocation = new StockStorageLocation();
        stockStorageLocation.Location = "filesystem2";

        var jsonContent = JsonContent.Create(stockStorageLocation);
        var response = await client.PostAsync("/StockSettings", jsonContent);

        Assert.AreNotEqual(savedStoragePath, _fileManager.StoragePath);

        // set back to "filesystem"
        stockStorageLocation.Location = "filesystem";
        jsonContent = JsonContent.Create(stockStorageLocation);
        response = await client.PostAsync("/StockSettings", jsonContent);

        Assert.AreEqual(savedStoragePath, _fileManager.StoragePath);

        Directory.Delete("filesystem2", true);
    }

}
