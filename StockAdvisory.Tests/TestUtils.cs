using System.IO;

namespace StockAdvisory.Tests;

public class TestUtils
{
    public const string CsvSourceUrl =
        "https://ark-funds.com/wp-content/uploads/funds-etf-csv/ARK_INNOVATION_ETF_ARKK_HOLDINGS.csv";
    
    public static void CleanDirectory(string path)
    {
        var dirInfo = new DirectoryInfo(path);
        var files = dirInfo.GetFiles();
        foreach (var file in files)
        {
            file.Delete();
        }
    }

    public static void RemoveDirectory(string path)
    {
        if (Directory.Exists(path))
        {
            Directory.Delete(path);
        }
    }
}