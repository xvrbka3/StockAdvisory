﻿using System;
using System.IO;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using NodaTime;
using NUnit.Framework;

namespace StockAdvisory.Tests;

public class FileManagerTests
{
    private static readonly string EmptyDir = "testFileSystem" + Path.DirectorySeparatorChar + "emptyDir";
    private static readonly string DownloadDir = "testFileSystem" + Path.DirectorySeparatorChar + "downloadDir";
    private static readonly string FewSources = "testFileSystem" + Path.DirectorySeparatorChar + "fewSources";
    private static readonly string InvalidEntries = "testFileSystem" + Path.DirectorySeparatorChar + "invalidEntries";

    private readonly HttpClient _client = new();

    [Test]
    public void Test_When_EmptyStorage_Then_NoRecordsFound()
    {
        var manager = new FileManager(EmptyDir, _client, TestUtils.CsvSourceUrl, SystemClock.Instance);
        Assert.IsEmpty(manager.ListAvailableRecords());
    }

    [Test]
    public async Task Test_When_DownloadFile_Then_FileExists()
    {
        var manager = new FileManager(DownloadDir, _client, TestUtils.CsvSourceUrl, SystemClock.Instance);
        var result = await manager.DownloadLatest();
        Assert.True(File.Exists(result.filePath));
        Assert.IsNotEmpty(manager.ListAvailableRecords());
        Assert.AreEqual(1, manager.ListAvailableRecords().Count);
        TestUtils.CleanDirectory(DownloadDir); // delete downloaded files
    }

    [Test]
    public void Test_When_InvalidEntriesExist_Then_TheyAreIgnored()
    {
        var validItems = new List<DateTime>()
        {
            new DateTime(2022, 01, 23, 11, 43, 00),
            new DateTime(2020, 05, 16, 01, 05, 00)
        };

        var manager = new FileManager(InvalidEntries, _client, TestUtils.CsvSourceUrl, SystemClock.Instance);
        var listedItems = manager.ListAvailableRecords();

        Assert.AreEqual(2, listedItems.Count);

        foreach (var item in validItems)
        {
            Assert.True(listedItems.Contains(item));
        }
    }

    [Test]
    public void Test_When_KnownEntriesExist_Then_TheyAreListed()
    {
        var validItems = new List<DateTime>()
        {
            new DateTime(2022, 01, 23, 11, 43, 00),
            new DateTime(2020, 05, 16, 01, 05, 00),
            new DateTime(2021, 12, 10, 06, 18, 00),
            new DateTime(2021, 11, 03, 18, 21, 00),
            new DateTime(2017, 08, 30, 22, 58, 00),
        };

        var manager = new FileManager(FewSources, _client, TestUtils.CsvSourceUrl, SystemClock.Instance);
        var listedItems = manager.ListAvailableRecords();

        Assert.AreEqual(5, listedItems.Count);

        foreach (var item in validItems)
        {
            Assert.True(listedItems.Contains(item));
        }

    }

    [Test]
    public void Test_When_StoragePathIsChanged_Then_CorrespondingDirectoryExists()
    {
        var manager = new FileManager(FewSources, _client, TestUtils.CsvSourceUrl, SystemClock.Instance);

        const string location = "newLocation";
        manager.SetStorageLocation(location);
        
        DirectoryAssert.Exists(location);
        
        TestUtils.RemoveDirectory(location);
    }
}