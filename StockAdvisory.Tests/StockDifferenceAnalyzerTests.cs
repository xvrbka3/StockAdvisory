using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using StockAdvisoryModels;

namespace StockAdvisory.Tests;

public class StockDifferenceAnalyzerTests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void Test_When_OneStockIsProvided_Then_OnlyLaterOneIsReturned()
    {
        StockEntryRecord previousRecord = new StockEntryRecord()
        {
            Date = new DateTime(2022, 3, 28), Company = "TESLA INC", Ticker = "TSLA",
            CuspId = "88160R101", Shares = 1_166_661, MarketValue = 1_179_074_273.04M, Weight = 100.00M
        };
        StockEntryRecord currentRecord = new StockEntryRecord()
        {
            Date = new DateTime(2022, 3, 31), Company = "TESLA INC", Ticker = "TSLA",
            CuspId = "88160R101", Shares = 1_129_150, MarketValue = 1_125_278_808.50M, Weight = 100.00M
        };
        var prevStocks = new List<StockEntryRecord>() {previousRecord};
        var currentStocks = new List<StockEntryRecord>() {currentRecord};
        var analyzer = new StockDifferenceAnalyzer(prevStocks, currentStocks);
        Assert.Contains(currentRecord, analyzer.AllStocks().ToList());
    }

    [Test]
    public void Test_When_NoStocksWereAddedOrRemoved_Then_EmptyCollectionIsReturned()
    {
        StockEntryRecord previousRecord = new StockEntryRecord()
        {
            Date = new DateTime(2022, 3, 28), Company = "TESLA INC", Ticker = "TSLA",
            CuspId = "88160R101", Shares = 1_166_661, MarketValue = 1_179_074_273.04M, Weight = 100.00M
        };
        StockEntryRecord currentRecord = new StockEntryRecord()
        {
            Date = new DateTime(2022, 3, 31), Company = "TESLA INC", Ticker = "TSLA",
            CuspId = "88160R101", Shares = 1_129_150, MarketValue = 1_125_278_808.50M, Weight = 100.00M
        };
        var prevStocks = new List<StockEntryRecord>() {previousRecord};
        var currentStocks = new List<StockEntryRecord>() {currentRecord};
        var analyzer = new StockDifferenceAnalyzer(prevStocks, currentStocks);
        Assert.IsEmpty(analyzer.AddedStocks());
        Assert.IsEmpty(analyzer.RemovedStocks());
    }

    [Test]
    public void Test_When_OneStockWasAdded_Then_OneStockIsReturned()
    {
        var addedStock = StaticTestData.LaterRecords.First(entry => entry.Ticker == "ZM");
        var analyzer = new StockDifferenceAnalyzer(StaticTestData.EarlierRecords, StaticTestData.LaterRecords);
        // Uncomment when implemented
        Assert.AreEqual(analyzer.AddedStocks().Count(), 1);
        CollectionAssert.Contains(analyzer.AddedStocks(), addedStock);
    }

    [Test]
    public void Test_When_OneStockWasRemoved_Then_OneStockIsReturned()
    {
        var removedStock = StaticTestData.EarlierRecords.First(entry => entry.Ticker == "COIN");
        var analyzer = new StockDifferenceAnalyzer(StaticTestData.EarlierRecords, StaticTestData.LaterRecords);
        // Uncomment when implemented
        Assert.AreEqual(analyzer.RemovedStocks().Count(), 1);
        CollectionAssert.Contains(analyzer.RemovedStocks(), removedStock);
    }

    [Test]
    public void Test_When_OneStockWasIncreased()
    {
        StockEntryRecord previousRecord = new StockEntryRecord()
        {
            Date = new DateTime(2022, 3, 28), Company = "TESLA INC", Ticker = "TSLA", CuspId = "88160R101",
            Shares = 1_166_661, MarketValue = 1_179_074_273.04M, Weight = 100.00M
        };
        StockEntryRecord currentRecord = new StockEntryRecord()
        {
            Date = new DateTime(2022, 3, 31), Company = "TESLA INC", Ticker = "TSLA", CuspId = "88160R101", 
            Shares = 2_333_322, MarketValue = 1_125_278_808.50M, Weight = 100.00M
        };
        var prevStocks = new List<StockEntryRecord>() { previousRecord };
        var currentStocks = new List<StockEntryRecord>() { currentRecord };
        var analyzer = new StockDifferenceAnalyzer(prevStocks, currentStocks);
        Assert.AreEqual(100, analyzer.ChangedPositions().First().SharesPercentChange);
    }
    
    [Test]
    public void Test_When_OneStockWasDecreased()
    {
        StockEntryRecord previousRecord = new StockEntryRecord()
        {
            Date = new DateTime(2022, 3, 28), Company = "TESLA INC", Ticker = "TSLA", CuspId = "88160R101",
            Shares = 2_333_322, MarketValue = 1_179_074_273.04M, Weight = 100.00M
        };
        StockEntryRecord currentRecord = new StockEntryRecord()
        {
            Date = new DateTime(2022, 3, 31), Company = "TESLA INC", Ticker = "TSLA", CuspId = "88160R101",
            Shares = 1_166_661, MarketValue = 1_125_278_808.50M, Weight = 100.00M
        };
        var prevStocks = new List<StockEntryRecord>() { previousRecord };
        var currentStocks = new List<StockEntryRecord>() { currentRecord };
        var analyzer = new StockDifferenceAnalyzer(prevStocks, currentStocks);
        Assert.AreEqual(-50f, analyzer.ChangedPositions().First().SharesPercentChange);
    }
    
    [Test]
    public void Test_When_MultipleStocksAnalyzedAndChanged()
    {
        var analyzer = new StockDifferenceAnalyzer(StaticStockData.EarlierRecords, StaticStockData.LaterRecords);
        var changedPositions = analyzer.ChangedPositions();
        Assert.AreEqual(3, changedPositions.Count());
    }

    [Test]
    public void Test_When_MultipleStocksAnalyzedAndNotChanged()
    {
        var analyzer = new StockDifferenceAnalyzer(StaticStockData.EarlierRecords, StaticStockData.EarlierRecords);
        Assert.AreEqual(0, analyzer.ChangedPositions().Count());
    }

    [Test]
    public void Test_When_StockDataContainsDuplicates()
    {
        var duplicateList = StaticStockData.EarlierRecords.Concat(StaticStockData.EarlierRecords).ToList();
        Assert.Throws<ArgumentException>(() =>
        {
            var _ = new StockDifferenceAnalyzer(duplicateList, StaticStockData.EarlierRecords);
        });
    }
}