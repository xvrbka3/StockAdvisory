using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;
using StockAdvisoryModels;

namespace StockAdvisory.Tests;

public class StockParserTests
{
    private readonly IStockParser _stockParser = new StockParser();
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void Test_When_StringWithSingleEntryIsPassed_Then_SingleEntryComesOut()
    {
        var parsedRecord = new StockEntryRecord()
        {
            Date = new DateTime(2022, 3, 31), Company = "TESLA INC", Ticker = "TSLA",
            CuspId = "88160R101", Shares = 1_129_150, MarketValue = 1_235_278_808.50M, Weight = 33.15M
        };

        var parsedEntries = _stockParser.Parse("csv/ark_mock_one_entry.csv");
        Assert.AreEqual(parsedEntries.Count, 1);
        Assert.Contains(parsedRecord, parsedEntries.ToList());
    }

    [Test]
    public void Test_When_StringWithFourEntriesIsPassed_Then_FourEntriesComeOut()
    {
        var parsedEntries = _stockParser.Parse("csv/mar31_four_entries.csv");
        CollectionAssert.AreEquivalent(StaticTestData.EarlierRecords, parsedEntries);
    }

    [Test]
    public void Test_When_RowIsIncorrect_Then_RowIsSkipped()
    {
        var expectedRecords = new List<StockEntryRecord>
        {
            new()
            {
                Date = new DateTime(2022, 3, 31), Company = "TESLA INC", Ticker = "TSLA",
                CuspId = "88160R101", Shares = 1_129_150, MarketValue = 1_235_278_808.50M, Weight = 33.15M
            },
            new()
            {
                Date = new DateTime(2022, 3, 31), Company = "UNITY SOFTWARE INC", Ticker = "U",
                CuspId = "91332U101", Shares = 5_937_727, MarketValue = 600_957_349.67M, Weight = 4.77M
            }
        };

        var parsedEntries = _stockParser.Parse("csv/ark_mock_incorrect_rows.csv");
        Assert.AreEqual(parsedEntries.Count, 2);
        CollectionAssert.AreEquivalent(expectedRecords, parsedEntries);
    }

    [Test]
    public void Test_When_FileNotFound_Then_ResultIsEmpty()
    {
        var result = _stockParser.Parse("csv/non-existing.csv");
        Assert.IsEmpty(result);
    }
}