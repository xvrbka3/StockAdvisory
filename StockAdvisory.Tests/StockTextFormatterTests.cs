using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using NUnit.Framework;
using StockAdvisoryModels;

namespace StockAdvisory.Tests;

public class StockTextFormatterTests
{
    private readonly StockEntryRecord _teslaRecord = new()
    {
        Date = new DateTime(2022, 3, 28), Company = "TESLA INC", Ticker = "TSLA",
        CuspId = "88160R101", Shares = 1_166_661, MarketValue = 1_179_074_273.04M, Weight = 100.00M
    };

    private readonly StockDifferenceRecord _spotifyDiffRecord = new()
    {
        Company = "SPOTIFY TECHNOLOGY SA", Ticker = "SPOT", CuspId = "L8681T102",
        Shares = 3_229_198, SharesPercentChange = (float) 4.01, Weight = 504_271_559
    };

    private static void CheckStringRecordContainingAllInformation(StockRecord expected, string formattedRecord)
    {
        StringAssert.Contains(expected.Company, formattedRecord);
        StringAssert.Contains(expected.Ticker, formattedRecord);
        StringAssert.Contains(expected.Shares.ToString(), formattedRecord);
        StringAssert.Contains(expected.Weight.ToString(CultureInfo.InvariantCulture),
            formattedRecord);
    }

    private static void CheckStringRecordWithAdditionalInfo(StockDifferenceRecord expected, string formattedRecord)
    {
        CheckStringRecordContainingAllInformation(expected, formattedRecord);
        StringAssert.Contains(expected.SharesPercentChange.ToString(CultureInfo.InvariantCulture), 
            formattedRecord);
    }

    [Test]
    public void Test_When_SingleStockEntryRecordIsProvided_Then_AllRelevantInformationIsContained()
    {
        var formatter = new StockTextFormatter();
        var formattedString = formatter.Format(_teslaRecord);
        CheckStringRecordContainingAllInformation(_teslaRecord, formattedString);
    }

    [Test]
    public void Test_When_CollectionWithSingleStockEntryIsProvided_Then_AllRelevantInformationIsContained()
    {
        var formatter = new StockTextFormatter();
        var records = new List<StockEntryRecord>() {_teslaRecord};
        var formattedString = formatter.Format(records);
        CheckStringRecordContainingAllInformation(_teslaRecord, formattedString);
    }

    [Test]
    public void Test_When_CollectionOfStockEntriesIsProvided_Then_OutputDoesNotStartWithEmptyLine()
    {
        var formatter = new StockTextFormatter();
        var records = new List<StockEntryRecord>() {_teslaRecord};
        var formattedString = formatter.Format(records);
        var stringReader = new StringReader(formattedString);
        var line = stringReader.ReadLine();
        Assert.NotNull(line);
        Assert.NotZero(line!.Trim().Length);
    }

    [Test]
    public void Test_When_StockDifferenceRecordIsProvided_Then_AllRelevantInformationIsContained()
    {
        var formatter = new StockTextFormatter();
        var formattedString = formatter.Format(_spotifyDiffRecord);
        CheckStringRecordWithAdditionalInfo(_spotifyDiffRecord, formattedString);
    }
    
    [Test]
    public void Test_When_CollectionWithSingleStockDiffRecordIsProvided_Then_AllRelevantInformationIsContained()
    {
        var formatter = new StockTextFormatter();
        var records = new List<StockDifferenceRecord>() {_spotifyDiffRecord};
        var formattedString = formatter.Format(records);
        CheckStringRecordWithAdditionalInfo(_spotifyDiffRecord, formattedString);
    }
}